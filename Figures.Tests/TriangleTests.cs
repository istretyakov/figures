﻿using Xunit;

namespace Figures.Tests
{
    public class TriangleTests
    {
        [Fact]
        public void Test()
        {
            for (int i = 0; i < triangles.Length; i++)
            {
                double result = triangles[i].CalculateSquare();

                Assert.Equal(triangleSquares[i], result);
            }
        }

        [Fact]
        public void IsRightWithRight()
        {
            Triangle t = new Triangle(3, 4, 5);

            bool result = t.IsRight();

            Assert.True(result);
        }

        [Fact]
        public void IsRightWithWrong()
        {
            Triangle t = new Triangle(2, 2, 2);

            bool result = t.IsRight();

            Assert.False(result);
        }

        private Triangle[] triangles = new Triangle[3]
        {
            new Triangle(2, 2, 3),
            new Triangle(3, 4, 5),
            new Triangle(5.5, 20.1243, 16.86562)
        };

        private double[] triangleSquares = new double[3]
        {
            1.984313483298443,
            6,
            40.51725428829939
        };
    }
}
