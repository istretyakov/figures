﻿using Xunit;

namespace Figures.Tests
{
    public class CircleTests
    {
        [Fact]
        public void Test()
        {
            Circle c = new Circle(5);

            double result = c.CalculateSquare();

            Assert.Equal(78.53981633974483, result);
        }
    }
}
