﻿using System;

namespace Figures
{
    public class Triangle : IFigure
    {
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }

        public Triangle(double a, double b, double c)
        {
            A = a;
            B = b;
            C = c;
        }

        public double CalculateSquare()
        {
            double p = (A + B + C) / 2;
            return Math.Sqrt(p * (p - A) * (p - B) * (p - C));
        }

        public bool IsRight()
        {
            if ((Math.Pow(A, 2) - (Math.Pow(B, 2) + Math.Pow(C, 2)) == 0.0) ||    // A^2 - (B^2 + C^2) = 0
                (Math.Pow(B, 2) - (Math.Pow(A, 2) + Math.Pow(C, 2)) == 0.0) ||    // B^2 - (A^2 + C^2) = 0
                (Math.Pow(C, 2) - (Math.Pow(A, 2) + Math.Pow(B, 2)) == 0.0))      // C^2 - (A^2 + B^2) = 0
                return true;

            return false;
        }
    }
}
