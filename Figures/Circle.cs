﻿using System;

namespace Figures
{
    public class Circle : IFigure
    {
        public double Radius { get; set; }

        public Circle(double r)
        {
            Radius = r;
        }

        public double CalculateSquare()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }
    }
}
